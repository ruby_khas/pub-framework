﻿using System;
using System.Collections.Generic;

using CJC.Framework.IO;


namespace CJC.Framework.Rule.Base
{
	public abstract class RuleExcuter : CRuleBranch
	{
		public RuleExcuter(string name, CRule rule, bool isMultiRoute, bool isBlocked, bool isMultiInput) : base(name, rule)
		{
			IsMultiRoute = isMultiRoute;
			IsBlocked = isBlocked;
			IsMultiInput = isMultiInput;
		}

		/// <summary>
		/// 在构造函数中注册route 并且之后不能改变。
		/// </summary>
		/// <param name="routeName"></param>
		protected void RegisterRoute(string routeName)
		{
			if (null == RuleRoutes)
				RuleRoutes = new List<RuleRoute>();

			if (null != GetRouteByName(routeName))
				return;

			RuleRoute route = new RuleRoute(routeName, this);
			RuleRoutes.Add(route);
		}

		/// <summary>
		/// 在构造函数中注册attribute，并且之后不能改变。
		/// </summary>
		/// <param name="attributeName"></param>
		protected void RegisterAttribute(string attributeName)
		{
			if (null == Attributes)
				Attributes = new Dictionary<string, string>();

			if (Attributes.ContainsKey(attributeName))
				return;

			Attributes.Add(attributeName, string.Empty);
		}


		public bool IsMultiInput { get; private set; }
		public bool IsBlocked { get; private set; }
		public bool IsMultiRoute { get; private set; }
		public List<RuleRoute> RuleRoutes;
		public Dictionary<string, string> Attributes;
		public void DoExecute(IModelData target)
		{
			if (IsBlocked) RuleManager.Instance.RunningIn(target, this);
			OnExecute(target);
		}

		public override bool XmlDeserilize(XmlData data)
		{
			BranchID = ToolParser.IntParse(data.GetAttribute(ERuleKey.ID));
			List<string> keys = new List<string>(Attributes.Keys);
			foreach (var attributeName in keys)
			{
				string value;
				if (!data.Attributes.TryGetValue(attributeName, out value))
					return false;

				Attributes[attributeName] = value;
			}
			return true;
		}

		protected abstract void OnExecute(IModelData target);

		protected void DoRoute(IModelData target, string routeName)
		{
			GetRouteByName(routeName).DoNext(target);
		}

		public RuleRoute GetRouteByName(string routeName)
		{
			return RuleRoutes.Find((_route) => { return _route.RouteName.Equals(routeName); });
		}

		// 离开这个组件
		public void DoExit(IModelData target)
		{
			ClearCache(target);
			if (IsBlocked) RuleManager.Instance.RunningOver(target, this);
		}

		// 包括 清除缓存 清除监听表等
		public virtual void ClearCache(IModelData target) { }

		public override bool XmlSerilize(XmlData exeData)
		{
			string[] typeNameArray = GetType().ToString().Split('.');
			string typeName = typeNameArray[typeNameArray.Length - 1];
			exeData.SetNodeName(typeName);

			return ExportAttributes(exeData);
		}

		protected abstract bool ExportAttributes(XmlData data);
		protected bool ExportAttribute(XmlData data, string attributeName)
		{
			string value;
			if (!Attributes.TryGetValue(attributeName, out value))
				return false;

			if (string.IsNullOrEmpty(value))
				return false;

			data.SetAttribute(attributeName, value);
			return true;
		}
	}

	public class RuleRoute : IXmlDataSerilizer
	{
		public RuleRoute(string routeName, RuleExcuter parent)
		{
			RouteName = routeName;
			Executer = parent;
			NextExcuters = new List<RuleExcuter>();
		}

		public string RouteName { get; private set; }
		public RuleExcuter Executer { get; private set; }
		public List<RuleExcuter> NextExcuters { get; private set; }

		public void DoNext(IModelData target)
		{
			foreach (var excuter in NextExcuters)
				excuter.DoExecute(target);
		}

		public bool XmlSerilize(XmlData routeData)
		{
			routeData.SetNodeName(RouteName);
			return true;
		}

		public bool XmlDeserilize(XmlData data) { return true; }
	}
}