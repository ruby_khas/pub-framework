﻿using System;
using CJC.Framework.IO;
using System.Collections.Generic;
using CJC.Framework.Rule;

namespace CJC.Framework.Rule.Base
{
	public class CRule: IXmlDataSerilizer
	{
		public int RuleID { get; private set; }
		public Dictionary<int, RuleExcuter> EnteranceExecuters { get; private set; }//仅在游戏运行时使用 编辑器下使用ruleObject.EnteranceExcuters;

		public CRule() { EnteranceExecuters = new Dictionary<int, RuleExcuter>(); }

		public void StartRule(IModelData target)
		{
			foreach(var executer in EnteranceExecuters.Values)
			{
				executer.DoExecute(target);
			}
		}

		public bool XmlSerilize(XmlData data)
		{
			data.AddAttribute(ERuleKey.ID, RuleID);
			return true;
		}

		/// <returns>是否为符合标准的rule格式</returns>
		public bool XmlDeserilize(XmlData data)
		{
			RuleID = ToolParser.IntParse(data.GetAttribute(ERuleKey.ID));
			return RuleID > 0;
		}

		public RuleExcuter CreateExecuter(XmlData data)
		{
			return CreateExecuter(data.Node);
		}


		public RuleExcuter CreateExecuter(string executerName)
		{
			RuleExcuter executer = CRuleBranch.CreateBranch(this, executerName) as RuleExcuter;
			if (null == executer)
				return null;

			return executer;
		}

#if UNITY_EDITOR
		public CRule(int id)
		{
			RuleID = id;
			EnteranceExecuters = new Dictionary<int, RuleExcuter>();
		}

		public void SetID(int id)
		{
			RuleID = id;
		}
#endif
	}

	public abstract class CRuleBranch: IXmlDataSerilizer
	{
		public string Name;
		public CRule Rule;
		public int RuleID { get { return Rule.RuleID; } }
		public int BranchID { get; protected set; }

		public CRuleBranch(string name, CRule rule) { Rule = rule; Name = name; }

		public abstract bool XmlSerilize(XmlData data);

		public abstract bool XmlDeserilize(XmlData data);

		public static CRuleBranch CreateBranch(CRule rule, string executerName)
		{
			Type type = Type.GetType("CJC.Framework.Rule." + executerName);
			CRuleBranch branch = Activator.CreateInstance(type, rule) as CRuleBranch;
			return branch;
		}
	}
}