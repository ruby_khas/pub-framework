﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using System;

public partial class RuleEditor
{
	private IObjectBase HandleObject;   // 判断鼠标持有的对象

	private void UpdateEvent()
	{
		RuleMouseEventBase ruleEvent = GetUpdateEvent(Event.current);

		if (null == ruleEvent) return;

		if(ruleEvent is RuleMouseDownEvent)
		{
			RuleMouseDownEvent e = ruleEvent as RuleMouseDownEvent;

			if(null != LeftMouseDownCallback)
			{
				LeftMouseDownCallback(e);

				return;
			}

			HandleObject = e.HandleTarget;

			Selection.activeObject = HandleObject;
		}
		else if(ruleEvent is RuleMouseDragEvent)
		{
			RuleMouseDragEvent e = ruleEvent as RuleMouseDragEvent;

			if (null != HandleObject)
			{
				Vector2 offset = e.CurrentPos - e.LastPos;
				HandleObject.Position += offset;
			}
		}
		else if(ruleEvent is RuleMouseUpEvent)
		{
			RuleMouseUpEvent e = ruleEvent as RuleMouseUpEvent;

			HandleObject = e.HandleTarget;
		}
		else if(ruleEvent is RuleContextEvent)
		{
			if (Event.current.mousePosition.x >= GetCutoffLineX())

				return;

			RuleContextEvent e = ruleEvent as RuleContextEvent;

			HandleObject = e.HandleTarget;

			Selection.activeObject = HandleObject;

			if (null == HandleObject)
			{
				GenericMenu menu = new GenericMenu();

				menu.AddItem(new GUIContent("新建/定时器"), false, CreateExecuter, new object[] { "ExeListenTimer", Event.current.mousePosition });

				menu.AddItem(new GUIContent("新建/比较"), false, CreateExecuter, new object[] { "ExeIfComparison", Event.current.mousePosition });

				menu.AddSeparator("");

				menu.ShowAsContext();

				Event.current.Use();
			}
			else if(HandleObject is ILineLinkable)
			{
				GenericMenu menu = new GenericMenu();

				menu.AddItem(new GUIContent("连接到..."), false, LinkLine,  HandleObject as ILineLinkable);

				menu.ShowAsContext();

				Event.current.Use();
			}
			else { }
		}
	}

	#region 注册鼠标事件
	private Action<RuleMouseDownEvent> LeftMouseDownCallback;
	#endregion

	#region 连线状态
	private ILineLinkable ActiveLinkable;
	private bool IsLinkLineMode;
	private void LinkLine(object data)
	{
		ActiveLinkable = data as ILineLinkable;
		IsLinkLineMode = true;
		LeftMouseDownCallback += LinkLine;
	}

	private void LinkLine(RuleMouseDownEvent e)
	{
		if(null != e.HandleTarget)
		{
			ExecuterObject executerObject = e.HandleTarget as ExecuterObject;
			
			if(null != executerObject)
			{
				ActiveLinkable.LinkLine(executerObject);
			}
		}
		ActiveLinkable = null;
		IsLinkLineMode = false;
		LeftMouseDownCallback -= LinkLine;
	}

	public interface ILineLinkable
	{
		bool LinkLine(ExecuterObject excuterObject);
		bool RemoveLinkLine(ExecuterObject excuterObject);
		void ClearLinkLine(); 
		Vector2 GetCenter();
	}
	#endregion

	private void CreateExecuter(object data)
	{
		if (null == SelectedRuleObject)
			return;

		object[] _params = (object[])data;
		SelectedRuleObject.CreateNewExecuter(_params[0].ToString(), (Vector2)_params[1]);
	}

	#region 封装坑爹的event事件
	private IObjectBase ___target;
	private Vector2 ___mLastPosition;   // 用于记录上一帧的鼠标位置
	private Vector2 ___mStartClickPosition;    // 记录鼠标单击下的时候 的鼠标位置
	private bool ___isMouseDown;   // 判断鼠标是否是落下状态

	public RuleMouseEventBase GetUpdateEvent(Event e)
	{
		if (null == SelectedRuleObject)
			return null;

		if (null == e)
			return null;

		if (e.button == 1 && e.type == EventType.ContextClick)
		{
			var pTarget = SelectedRuleObject.TryGetObjectAtPos(e.mousePosition);
			RuleContextEvent ruleContextEvent = new RuleContextEvent(e.mousePosition, pTarget);
			return ruleContextEvent;
		}

		if (e.button != 0)
			return null;

		if(mouseOverWindow != this)
		{
			RuleMouseEventBase eventClear = CheckOutWindow();
			___isMouseDown = false;
			___target = null;
			return eventClear;
		}

		switch(e.type)
		{
			case EventType.MouseDown:
				{
					___isMouseDown = true;
					___mStartClickPosition = e.mousePosition;
					___mLastPosition = e.mousePosition;

					___target = SelectedRuleObject.TryGetObjectAtPos(e.mousePosition);
					RuleMouseDownEvent downEvent = new RuleMouseDownEvent(e.mousePosition, ___target);
					return downEvent;
				}
			case EventType.MouseUp:
				{
					___target = SelectedRuleObject.TryGetObjectAtPos(e.mousePosition);
					RuleMouseUpEvent upEvent = new RuleMouseUpEvent(___mStartClickPosition, e.mousePosition, ___target);
					___isMouseDown = false;
					return upEvent;
				}
			case EventType.MouseDrag:
				{
					if(___isMouseDown && ___target != null)
					{
						RuleMouseDragEvent dragEvent = new RuleMouseDragEvent(___mLastPosition, e.mousePosition, ___target);
						___mLastPosition = e.mousePosition;
						return dragEvent;
					}
					return null;
				}
		}
		return null;
	}

	private RuleMouseEventBase CheckOutWindow()
	{
		if (!___isMouseDown)
			return null;

		if (null == ___target)
			return null;

		RuleOutWindowEvent outEvent = new RuleOutWindowEvent(___mStartClickPosition, ___mLastPosition, ___target);
		return outEvent;
	}

	public abstract class RuleMouseEventBase { }

	public class RuleMouseUpEvent : RuleMouseEventBase
	{
		public readonly Vector2 DownPos;
		public readonly Vector2 UpPos;
		public readonly IObjectBase HandleTarget;

		public RuleMouseUpEvent(Vector2 downPos, Vector2 upPos, IObjectBase target)
		{
			DownPos = downPos;
			UpPos = upPos;
			HandleTarget = target;
		}
	}

	public class RuleMouseDownEvent : RuleMouseEventBase
	{
		public readonly Vector2 DownPos;
		public readonly IObjectBase HandleTarget;

		public RuleMouseDownEvent(Vector2 downPos, IObjectBase target)
		{
			DownPos = downPos;
			HandleTarget = target;
		}
	}

	public class RuleMouseClickEvent : RuleMouseEventBase { }//有需求再做，判断点下和起来是同一个对象

	public class RuleMouseDragEvent : RuleMouseEventBase
	{
		public readonly Vector2 LastPos;
		public readonly Vector2 CurrentPos;
		public readonly IObjectBase HandleTarget;

		public RuleMouseDragEvent(Vector2 lastPos, Vector2 curPos, IObjectBase target)
		{
			LastPos = lastPos;
			CurrentPos = curPos;
			HandleTarget = target;
		}
	}

	public class RuleOutWindowEvent : RuleMouseEventBase
	{
		public readonly Vector2 DownPos;
		public readonly Vector2 OutWindowPos;
		public readonly IObjectBase HandleTarget;

		public RuleOutWindowEvent(Vector2 downPos, Vector2 outWindowPos, IObjectBase target)
		{
			DownPos = downPos;
			OutWindowPos = outWindowPos;
			HandleTarget = target;
		}
	}

	public class RuleContextEvent: RuleMouseEventBase
	{
		public readonly Vector2 Position;
		public readonly IObjectBase HandleTarget;

		public RuleContextEvent(Vector2 position, IObjectBase target)
		{
			Position = position;
			HandleTarget = target;
		}
	}
	#endregion
}