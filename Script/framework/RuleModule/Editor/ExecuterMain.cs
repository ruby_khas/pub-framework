﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class ExecuterMain : IObjectBase, RuleEditor.ILineLinkable
{
	public const float Radius = 20;
	private float Diameter { get { return Radius * 2; } }

	public Rect Rect { get { return new Rect(Position.x - Radius, Position.y - Radius, Diameter, Diameter); } }

	public ExecuterMain()
	{
		Position = new Vector2(50, 50);
	}

	public RuleObject RuleHost;

	public override void Print()
	{
		foreach (var executer in RuleHost.EnteranceExecuters)
		{
			Handles.DrawAAPolyLine(executer.Center, Position);
		}
		Color color = Handles.color;
		Handles.color = Color.gray;
		Handles.DrawSolidDisc(Position, Vector3.forward, Radius);
		Handles.color = color;
		GUI.Label(Rect, "Main", GUIStyleManager.Instance.LableStyle);
	}

	public void InitHost(RuleObject ruleHost)
	{
		RuleHost = ruleHost;
	}

	public override bool XmlDeserilize(XmlData data) { return true; }

	public override bool XmlSerilize(XmlData data) { return true; }

	public Vector2 GetCenter() { return Position; }

	public bool LinkLine(ExecuterObject executerObject)
	{
		if (null == executerObject)
			return false;

		if (!executerObject.LinkTarget(this))
			return false;

		RuleHost.EnteranceExecuters.Add(executerObject);
		return true;
	}

	public bool RemoveLinkLine(ExecuterObject excuterObject)
	{
		if (!RuleHost.EnteranceExecuters.Contains(excuterObject))
			return false;

		if (!excuterObject.RemoveLinkLine(this))
			return false;

		RuleHost.EnteranceExecuters.Remove(excuterObject);
		return true;
	}

	public void ClearLinkLine() { }
}
