﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

[CustomEditor(typeof(ExecuterObject))]
public class ExecuterObjectEditor : Editor
{
	public override void OnInspectorGUI()
	{
		m_changedKey = null;
		m_changedValue = null;

		ExecuterObject executerObject = (ExecuterObject)target;

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("位置");
		float x = EditorGUILayout.FloatField(executerObject.Position.x);
		float y = EditorGUILayout.FloatField(executerObject.Position.y);
		executerObject.Position = new Vector2(x, y);
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.BeginHorizontal();
		EditorGUILayout.LabelField("长宽");
		executerObject.Size.x = EditorGUILayout.FloatField(executerObject.Size.x);
		executerObject.Size.y = EditorGUILayout.FloatField(executerObject.Size.y);
		EditorGUILayout.EndHorizontal();

		EditorGUILayout.Space();
		EditorGUILayout.LabelField("属性");
		foreach (var attribute in executerObject.Attributes)
		{
			EditorGUILayout.BeginHorizontal();
			EditorGUILayout.Space();
			EditorGUILayout.LabelField(attribute.Key);
			string value = EditorGUILayout.TextField(attribute.Value);
			if(!value.Equals(attribute.Value))
			{
				m_changedKey = attribute.Key;
				m_changedValue = value;
			}
			EditorGUILayout.EndHorizontal();
		}

		if(!string.IsNullOrEmpty(m_changedKey))
			executerObject.Attributes[m_changedKey] = m_changedValue;
	}

	private string m_changedKey;
	private string m_changedValue;
}
