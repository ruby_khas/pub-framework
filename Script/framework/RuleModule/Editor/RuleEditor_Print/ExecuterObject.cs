﻿using UnityEngine;
using System.Collections;
using System;
using CJC.Framework.Rule.Base;
using System.Collections.Generic;
using UnityEditor;

public class ExecuterObject : IObjectBase
{
	public Vector2 Size;
	public Dictionary<string, string> Attributes;

	public RuleExcuter ExecuterHost;
	public RuleObject RuleHost;

	public List<RouteObject> RouteObjects;

	private List<RuleEditor.ILineLinkable> LinkedTargets;

	public Rect Rect { get { return new Rect(Position, Size); } }

	public Vector2 Center { get { return Position + Size / 2; } }

	public ExecuterObject() { Size = new Vector2(80, 40); RouteObjects = new List<RouteObject>(); LinkedTargets = new List<RuleEditor.ILineLinkable>(); }

	public void InitHost(RuleObject ruleHost, RuleExcuter executerHost)
	{
		RuleHost = ruleHost;

		ExecuterHost = executerHost;
		Attributes = executerHost.Attributes;

		foreach(var route in executerHost.RuleRoutes)
		{
			RouteObject routeObject = CreateInstance<RouteObject>();
			routeObject.InitHost(this, route);
			RouteObjects.Add(routeObject);
		}
	}

	public IObjectBase TryGetObjectAtPos(Vector2 position)
	{
		foreach(var route in RouteObjects)
		{
			if (route.Rect.Contains(position))
				return route;
		}
		if (Rect.Contains(position))
			return this;

		return null;
	}

	public bool LinkTarget(RuleEditor.ILineLinkable target)
	{
		if (!ExecuterHost.IsMultiInput)
		{
			if (LinkedTargets.Count >= 1)
				return false;
		}
		LinkedTargets.Add(target);
		return true;
	}

	public bool RemoveLinkLine(RuleEditor.ILineLinkable target)
	{
		return LinkedTargets.Remove(target);
	}

	public RouteObject GetRouteByName(string routeName)
	{
		return RouteObjects.Find((ruleObject) => { return ruleObject.RouteHost.RouteName.Equals(routeName); });
	}

	public override void Print()
	{
		Handles.DrawSolidRectangleWithOutline(Rect, Color.gray, Color.black);
		GUI.Label(Rect, ExecuterHost.Name, GUIStyleManager.Instance.LableStyle);

		foreach (var route in RouteObjects)
			route.Print();
	}

	public override bool XmlSerilize(XmlData data)
	{
		data.SetAttribute(ERuleEditorKey.Position, ToolParser.StringParser(Position));
		return true;
	}

	public override bool XmlDeserilize(XmlData data)
	{
		Position = ToolParser.VectorParser(data.GetAttribute(ERuleEditorKey.Position));
		return true;
	}
}
