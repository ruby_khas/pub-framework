﻿using UnityEngine;
using System.Collections;
using System;
using CJC.Framework.Rule.Base;
using System.Collections.Generic;
using CJC.Framework.Rule;
using UnityEditor;

public class RuleObject : IObjectBase
{
	public CRule RuleHost;
	public List<ExecuterObject> Executers;
	public List<ExecuterObject> EnteranceExecuters;

	public ExecuterMain ExecuterMain;

	public override bool XmlSerilize(XmlData ruleData)
	{
		ruleData.SetAttribute(ERuleEditorKey.Position, ToolParser.StringParser(Position));
		return true;
	}


	public override bool XmlDeserilize(XmlData data)
	{
		Position = ToolParser.VectorParser(data.GetAttribute(ERuleEditorKey.Position));
		return true;
	}

	public void CreateNewExecuter(string executerName, Vector2 position)
	{
		RuleExcuter executer = RuleHost.CreateExecuter(executerName);
		if (null == executer)
			return;

		ExecuterObject executerObject = CreateInstance<ExecuterObject>();
		executerObject.InitHost(this, executer);
		executerObject.Position = position;
		Executers.Add(executerObject);
	}

	public override void Print()
	{
		if(null == ExecuterMain)
		{
			ExecuterMain = CreateInstance<ExecuterMain>();
			ExecuterMain.InitHost(this);
		}

		ExecuterMain.Print();

		foreach(var executer in Executers)
		{
			foreach(var route in executer.RouteObjects)
			{
				foreach(var linkedTarget in route.ExecuterObjects)
				{
					Handles.DrawAAPolyLine(route.Center, linkedTarget.Center);
				}
			}
		}

		foreach(var executer in Executers)
		{
			executer.Print();
		}
	}

	public RuleObject()
	{
		RuleHost = new CRule(0);
		Executers = new List<ExecuterObject>();
		EnteranceExecuters = new List<ExecuterObject>();
	}

	private void SortExecutersByDepth() { }

	public IObjectBase TryGetObjectAtPos(Vector2 position)
	{
		SortExecutersByDepth();

		IObjectBase result = null;
		foreach(var executer in Executers)
		{
			result = executer.TryGetObjectAtPos(position);
			if (null != result)
				return result;
		}

		if (Vector2.Distance(position, ExecuterMain.Position) < ExecuterMain.Radius)
			return ExecuterMain;
		
		return null;
	}
}