﻿using CJC.Framework.Rule.Base;
using System;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

public class RouteObject : IObjectBase, RuleEditor.ILineLinkable
{
	private float Radius = 5f;

	private float Diameter { get { return Radius * 2; } }


	public override Vector2 Position
	{
		get
		{
			return Center - new Vector2(Radius, Radius);
		}
		set { }
	}

	public Vector2 Center
	{
		get {
			return ExecuterHost.Position
				+ new Vector2(ExecuterHost.Size.x, ExecuterHost.Size.y / ExecuterHost.ExecuterHost.RuleRoutes.Count * index);
		}
	}

	public RouteObject() { ExecuterObjects = new List<ExecuterObject>(); }

	public Rect Rect { get { return new Rect(Position, new Vector2(Diameter, Diameter)); } }

	private int index { get { return ExecuterHost.ExecuterHost.RuleRoutes.IndexOf(RouteHost); } }

	public ExecuterObject ExecuterHost;
	public RuleRoute RouteHost;

	public List<ExecuterObject> ExecuterObjects { get; set; }

	public void InitHost(ExecuterObject executerObject, RuleRoute ruleRoute)
	{
		ExecuterHost = executerObject;
		RouteHost = ruleRoute;
	}

	public override void Print()
	{
		Handles.DrawSolidDisc(Center, Vector3.forward, Radius);
	}

	public override bool XmlDeserilize(XmlData data) { return true; }
	public override bool XmlSerilize(XmlData data) { return true; }

	public Vector2 GetCenter() { return Center; }

	public bool LinkLine(ExecuterObject executerObject)
	{
		if (null == executerObject)
			return false;

		if (!executerObject.LinkTarget(this))
			return false;

		ExecuterObjects.Add(executerObject);
		return true;
	}

	public bool RemoveLinkLine(ExecuterObject excuterObject)
	{
		if (!ExecuterObjects.Contains(excuterObject))
			return false;

		if (!excuterObject.RemoveLinkLine(this))
			return false;

		ExecuterObjects.Remove(excuterObject);
		return true;
	}

	public void ClearLinkLine()
	{
		foreach(var executerObject in ExecuterObjects)
		{
			executerObject.RemoveLinkLine(this);
		}
		ExecuterObjects.Clear();
	}
}
