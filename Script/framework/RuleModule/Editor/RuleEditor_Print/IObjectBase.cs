﻿using UnityEngine;
using System.Collections;
using System;
using CJC.Framework.IO;

public abstract class IObjectBase : ScriptableObject, IXmlDataSerilizer
{
	private long Depth; // 简单的操作一次递加就好
	public virtual Vector2 Position { get; set; }

	public abstract void Print();

	public abstract bool XmlSerilize(XmlData data);

	public abstract bool XmlDeserilize(XmlData data);


	public virtual void Update() { }
}

public class ERuleEditorKey
{
	public const string Position = "position";
	public const string RuleExecuterRest = "rest";
}