﻿using CJC.Framework.Rule.Base;
using System.Collections;
using System.Collections.Generic;
using UnityEditor;
using UnityEngine;

/// <summary>
/// 编辑器的导出，保存都是基于编辑器的脚本 xxxObject， 只有导入游戏运行版本，基于脚本RuleExecuter
/// </summary>
public partial class RuleManager
{
    public static bool SerilizerRuleEditor(RuleObject ruleObject, out XmlData data)
	{
		data = new XmlData();
		ruleObject.RuleHost.XmlSerilize(data);
		ruleObject.XmlSerilize(data);

		XmlData main = new XmlData();
		main.SetNodeName(ERuleKey.Main);
		data.AddChild(main);

		foreach (var executer in ruleObject.EnteranceExecuters)
		{
			XmlData exeData;
			SerilizerExecuterEditor(executer, out exeData);

			if (!XmlData.IsEmpty(exeData))
				main.AddChild(exeData);
		}

		XmlData rest;
		XmlSerlizeRest(ruleObject, out rest);

		if (!XmlData.IsEmpty(rest))
			data.AddChild(rest);

		return true;
	}

	private static bool XmlSerlizeRest(RuleObject ruleObject, out XmlData rest)
	{
		rest = null;
		if (null == ruleObject.Executers)
			return true;

		List<ExecuterObject> pExecuterMap = new List<ExecuterObject>();
		for (int idx = 0; idx < ruleObject.Executers.Count; ++idx)
		{
			pExecuterMap.Add(ruleObject.Executers[idx]);
		}
		foreach (var executerObject in ruleObject.Executers)
		{
			foreach (var route in executerObject.RouteObjects)
			{
				if (null == route.ExecuterObjects)
					continue;

				foreach (var linkedExecuter in route.ExecuterObjects)
					pExecuterMap.Remove(linkedExecuter);
			}
		}

		foreach (var executer in ruleObject.EnteranceExecuters)
		{
			pExecuterMap.Remove(executer);
		}

		if (0 == pExecuterMap.Count)
			return true;

		rest = new XmlData();
		rest.SetNodeName(ERuleEditorKey.RuleExecuterRest);
		// 经过上面的操作 得到了，没有任何父连线的头executer map。
		foreach (var executerObject in pExecuterMap)
		{
			XmlData exeData;
			SerilizerExecuterEditor(executerObject, out exeData);

			if (!XmlData.IsEmpty(exeData))
				rest.AddChild(exeData);
		}
		return true;
	}

	private static bool SerilizerExecuterEditor(ExecuterObject executer, out XmlData data)
	{
		data = new XmlData();
		executer.XmlSerilize(data);
		executer.ExecuterHost.XmlSerilize(data);

		foreach(var route in executer.RouteObjects)
		{
			XmlData routeData;
			SerilizerRouteEditor(route, out routeData);

			if (!XmlData.IsEmpty(routeData))
				data.AddChild(routeData);
		}
		return true;
	}

	private static bool SerilizerRouteEditor(RouteObject route, out XmlData data)
	{
		data = new XmlData();
		route.XmlSerilize(data);
		route.RouteHost.XmlSerilize(data);

		foreach(var executer in route.ExecuterObjects)
		{
			XmlData exeData;
			SerilizerExecuterEditor(executer, out exeData);

			if (!XmlData.IsEmpty(exeData))
				data.AddChild(exeData);
		}
		return true;
	}

	public static bool SerilizerRule(RuleObject ruleObject, out XmlData data)
	{
		data = new XmlData();
		if (!ruleObject.RuleHost.XmlSerilize(data))
			return false;

		XmlData main = new XmlData();
		main.SetNodeName(ERuleKey.Main);
		data.AddChild(main);

		foreach (var executer in ruleObject.EnteranceExecuters)
		{
			XmlData exeData;
			if (!SerilizerExecuter(executer, out exeData))
				return false;

			if (!XmlData.IsEmpty(exeData))
				main.AddChild(exeData);
		}
		return true;
	}

	private static bool SerilizerExecuter(ExecuterObject executerObject, out XmlData data)
	{
		data = new XmlData();
		if (!executerObject.ExecuterHost.XmlSerilize(data))
			return false;

		foreach (var route in executerObject.RouteObjects)
		{
			XmlData routeData;
			if (!SerilizerRoute(route, out routeData))
				return false;

			if (!XmlData.IsEmpty(routeData))
				data.AddChild(routeData);
		}
		return true;
	}

	private static bool SerilizerRoute(RouteObject route, out XmlData data)
	{
		data = new XmlData();
		if (!route.XmlSerilize(data))
			return false;

		if (!route.RouteHost.XmlSerilize(data))
			return false;

		foreach (var executer in route.ExecuterObjects)
		{
			XmlData exeData;
			if (!SerilizerExecuter(executer, out exeData))
				return false;

			if (!XmlData.IsEmpty(exeData))
				data.AddChild(exeData);
		}
		return true;
	}

	public static bool DeserilizerRuleEditor(XmlData data, out RuleObject ruleObject)
	{
		ruleObject = ScriptableObject.CreateInstance<RuleObject>();
		ruleObject.RuleHost.XmlDeserilize(data);
		ruleObject.XmlDeserilize(data);

		XmlData main = data.SearchChild(ERuleKey.Main);

		if (null == main)
			return false;

		foreach (var exeData in main.Childs)
		{
			ExecuterObject executerObject;
			DeserilizerExecuterEditor(exeData, ruleObject, out executerObject);
			ruleObject.EnteranceExecuters.Add(executerObject);
		}

		XmlData rest = data.SearchChild(ERuleEditorKey.RuleExecuterRest);
		if (null == rest)
			return true;

		foreach (var exeData in rest.Childs)
		{
			ExecuterObject executerObject;
			DeserilizerExecuterEditor(exeData, ruleObject, out executerObject);
		}
		return true;
	}

	private static bool DeserilizerExecuterEditor(XmlData data, RuleObject ruleObject, out ExecuterObject executerObject)
	{
		RuleExcuter executer = ruleObject.RuleHost.CreateExecuter(data);
		executer.XmlDeserilize(data);

		executerObject = ScriptableObject.CreateInstance<ExecuterObject>();
		executerObject.InitHost(ruleObject, executer);
		executerObject.XmlDeserilize(data);
		ruleObject.Executers.Add(executerObject);

		foreach (var routeData in data.Childs)
		{
			RouteObject routeObject;
			DeserilizerRouteEditor(routeData, executerObject, out routeObject);
			executerObject.RouteObjects.Add(routeObject);
		}
		return true;
	}

	private static bool DeserilizerRouteEditor(XmlData data, ExecuterObject executerObject, out RouteObject routeObject)
	{
		string routeName = data.Node;
		routeObject = executerObject.GetRouteByName(routeName);

		if (null == routeObject)
			return false;

		routeObject.RouteHost.XmlDeserilize(data);
		routeObject.XmlDeserilize(data);

		foreach(var exeData in data.Childs)
		{
			ExecuterObject exeObject;
			DeserilizerExecuterEditor(exeData, executerObject.RuleHost, out exeObject);
			routeObject.LinkLine(exeObject);
		}
		return true;
	}

	public static bool DeserilizerRule(XmlData data, out CRule rule)
	{
		rule = new CRule();
		if (!rule.XmlDeserilize(data))
			return false;

		XmlData main = data.SearchChild(ERuleKey.Main);

		if (null == main)
			return false;

		foreach (var exeData in main.Childs)
		{
			RuleExcuter executer;
			if (!DeserilizerExecuter(exeData, rule, out executer))
				return false;

			rule.EnteranceExecuters.Add(executer.BranchID, executer);
		}
		return true;
	}

	private static bool DeserilizerExecuter(XmlData data, CRule rule, out RuleExcuter executer)
	{
		executer = rule.CreateExecuter(data);
		if (!executer.XmlDeserilize(data))
			return false;

		foreach (var routeData in data.Childs)
		{
			if (!DeserilizerRoute(routeData, executer))
				return false;
		}
		return true;
	}

	private static bool DeserilizerRoute(XmlData data, RuleExcuter executer)
	{
		string routeName = data.Node;
		RuleRoute route = executer.GetRouteByName(routeName);

		if (null == route)
			return false;

		if (!route.XmlDeserilize(data))
			return false;

		foreach (var exeData in data.Childs)
		{
			RuleExcuter _executer;
			if (!DeserilizerExecuter(exeData, executer.Rule, out _executer))
				return false;

			route.NextExcuters.Add(_executer);
		}
		return true;
	}
}
