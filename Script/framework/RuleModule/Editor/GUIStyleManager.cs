﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GUIStyleManager
{
	private static GUIStyleManager _instance;
	public static GUIStyleManager Instance
	{
		get {
			if (null == _instance)
			{
				_instance = new GUIStyleManager();
			}
			return _instance;
		}
	}

	private GUIStyleManager()
	{
		LableStyle = new GUIStyle();
		LableStyle.alignment = TextAnchor.MiddleCenter;
	}

	public GUIStyle LableStyle;
}
