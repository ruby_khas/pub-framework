﻿
public class GUText : BaseGUI
{
    public override GUIType GetUIType { get { return GUIType.Text; } }

    public UnityEngine.UI.Text text;
}
